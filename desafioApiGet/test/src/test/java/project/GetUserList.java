package project;

import config.Configuracoes;
import org.aeonbits.owner.ConfigFactory;
import org.junit.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.equalTo;
import static io.restassured.module.jsv.JsonSchemaValidator.*;


public class GetUserList {

    @Test
    public void getRetornaUsuario(){
        Configuracoes configuracoes = ConfigFactory.create(Configuracoes.class);
        baseURI = configuracoes.baseURI();

        when()
            .get(baseURI)
        .then()
            .assertThat()
                .statusCode(200)
                .body(matchesJsonSchemaInClasspath("schemas/getUserList.json"))
                .body("address.city", equalTo("São Paulo"))
                .body("address.street", equalTo("Avenida das Oliveiras"))
                .body("address.number", equalTo(99))
                .body("address.UF", equalTo("SP"));
//                .body("users.name[1]", equalTo("Margareth"));
//                .body("address.geo.state.lat", equalTo("4514.1353"))
//                .body("users.geo.state.long", equalTo("15132534"))
//                .body("users.geo.state.planet", equalTo("Earth"))
//                .body("address.others", equalTo("AP401"))

    }
}
