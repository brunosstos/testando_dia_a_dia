package config;

import org.aeonbits.owner.Config;

@Config.Sources({"file:src/test/resources/properties/setup.properties"})
public interface Configuracoes extends Config{

    @Key("baseURI")
    String baseURI();

}
